<?php


class Declination {

    private $arrayWord;

    function __construct()
    {
    }

    function setWords($array)
    {

            $this->arrayWord = $array;

    }

    function getDeclination($number){

        if($number == 0) return 0;

        if($number % 100 > 4 && $number % 100 < 21) {

            return $number . " " . $this->arrayWord[2];

        } else
            if($number % 100 == 1) {

                return $number . " " . $this->arrayWord[0];

            } else
                $i = $number % 10;
                switch ($i){

                    case (1) :
                        return $number." ".$this->arrayWord[0]; break;
                    case (2) :
                    case (3) :
                    case (4) :
                        return $number." ".$this->arrayWord[1]; break;
                    default : return $number." ".$this->arrayWord[2];

                }
    }

}

$array = [
    "отзыв",
    "отзыва",
    "отзывов"
];

$a = new Declination();
$a->setWords($array);

echo $a->getDeclination(101);